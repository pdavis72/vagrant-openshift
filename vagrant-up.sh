#!/bin/bash

# vagrant-libvrt and/or landrush together have issues
# with starting multiple machines simultaneously. 
# This script just helps start them one-by-one.
vagrant up lb &&
vagrant up nfs &&
vagrant up lbext &&
vagrant up master1 &&
vagrant up master2 &&
vagrant up master3 &&
vagrant up infra1 &&
vagrant up infra2 &&
vagrant up node1 &&
vagrant up node2
