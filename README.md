# vagrant-openshift

OpenShift Container Platform 3.5 Vagrant deployment

https://docs.openshift.com

## Tools

https://www.ansible.com/  
https://docs.openshift.com/container-platform/3.5/welcome/index.html  
https://libvirt.org/drvqemu.html  
https://www.vagrantup.com/  
https://github.com/devopsgroup-io/vagrant-hostmanager  
https://github.com/vagrant-landrush/landrush  
https://github.com/vagrant-libvirt/vagrant-libvirt  

## Use case

This is meant to be an easy to create and tear down environment for testing OpenShift Container Platform.

It is currently setup to deploy OCP 3.5 using RHEL 7 virtual machines provided by `vagrant up`.

It is also possible to use this repository to deploy OpenShift Origin on top of Centos machines instead, as that was the original goal. The repository shifted to an OCP based deployment though, and some minor tweaking of the Playbooks and the `Vagrantfile` will be needed to get Origin running.

## Host machine

The Host machine tested on had 32GB of available RAM.

Vagrant is configured to use the [vagrant-libvirt](https://github.com/vagrant-libvirt/vagrant-libvirt) driver for provisioning the VMs. The Host machine had libvirt working with QEMU/KVM drivers.

To be able to reach the provisioned load balancers from the outside world, port forwarding was setup from network gear to the respective VMs.

## Topology

```
lb      - Load balancer to Master nodes
lbext   - Load balancer to Infra nodes
nfs     - NFS server
master1 - Master node
master2 - Master node
master3 - Master node
infra1  - Infra node
infra2  - Infra node
node1   - Application node
node2   - Application node
```

## Setup

1. Bring up virtual machines

  ```shell
  ./vagrant-up.sh
  ```

2. Setup RHEL subscription credentials

  ```shell
  # Create the credentials file for RHEL subscription-manager
  touch ansible/playbooks/credentials.yml

  cat > ansible/playbooks/credentials.yml <<EOL
  username: ""
  password: ""
  pool: ""
  EOL

  # Edit the above file and put in your credentials
  ```

3. Provision the VMs

  ```shell
  ./run_ansible.sh
  ```

4. Run the OCP advanced installation

  ```shell
  vagrant ssh master1

  ansible-playbook /usr/share/ansible/openshift-ansible/playbooks/byo/config.yml
  ```
