#!/bin/bash

ansible-playbook -i ansible/hosts ansible/playbooks/00_rhel-registration-playbook.yml &&
ansible-playbook -i ansible/hosts ansible/playbooks/01_prereq-playbook-enterprise.yml &&
ansible-playbook -i ansible/hosts ansible/playbooks/02_docker-playbook.yml &&
ansible-playbook -i ansible/hosts ansible/playbooks/03_ssh-keys-playbook.yml &&
ansible-playbook -i ansible/hosts ansible/playbooks/04_ose-setup-playbook.yml &&
ansible-playbook -i ansible/hosts ansible/playbooks/05_setup-haproxy.yml &&
ansible-playbook -i ansible/hosts ansible/playbooks/07-network-manager-enable.yml
