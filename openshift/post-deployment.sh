#!/bin/bash

# Bug fix of logging project with < v3.5.94
oc project logging
oc edit configmap logging-elasticsearch
# Follow steps at:
# https://access.redhat.com/solutions/3110981
# io.fabric8.elasticsearch.kibana.mapping.empty: /usr/share/elasticsearch/index_patterns/com.redhat.viaq-openshift.index-pattern.json

oc annotate namespace default openshift.io/node-selector="region=primary" --overwrite

# Setup logging project curator
# We wan't to delete the any logs older than 1 day old daily
oc replace -f logging/curator.yaml

oc rollout latest dc/logging-curator
oc rollout latest dc/logging-curator-ops
