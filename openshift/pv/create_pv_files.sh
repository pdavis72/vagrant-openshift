#!/bin/bash

# Create the PV templates locally
for i in {01..10}; do
cat > pv_${i}.yaml <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-pv${i}
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  nfs:
    path: /exports/pv/nfs-pv_${i}
    server: nfs
    readOnly: false
EOF
done;

# RWO PVs
for i in {11..15}; do
cat > pv_${i}.yaml <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-pv${i}
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  nfs:
    path: /exports/pv/nfs-pv_${i}
    server: nfs
    readOnly: false
EOF
done;

exit;

# On the NFS server, we need to create the directories that the PVs will use
for i in {01..10}; do
  sudo mkdir -p /exports/pv/nfs-pv_${i}

  # Setup the exportfs configuration
  sudo echo "/exports/pv/nfs-pv_${i}   *(rw,sync,root_squash,no_all_squash)" >> /etc/exports
done

for i in {11..15}; do
  sudo mkdir -p /exports/pv/nfs-pv_${i}

  # Setup the exportfs configuration
  sudo echo "/exports/pv/nfs-pv_${i}   *(rw,sync,root_squash,no_all_squash)" >> /etc/exports
done

sudo chown -R nfsnobody:nfsnobody /exports/pv
sudo chmod -R 777 /exports/pv
sudo exportfs -ra
exportfs
