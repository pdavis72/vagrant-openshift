# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "homeski/rhel7.3"
  # https://github.com/vagrant-libvirt/vagrant-libvirt
  provider = "libvirt"

  config.ssh.private_key_path = "ansible/vagrant_private_key"
  config.ssh.insert_key = false

  config.vm.synced_folder ".", "/vagrant", disabled: true

  # https://github.com/devopsgroup-io/vagrant-hostmanager
  config.hostmanager.enabled = true

  # https://github.com/vagrant-landrush/landrush
  config.landrush.enabled = true
  # This should be the default..but set it just in case
  config.landrush.upstream "8.8.8.8"
  # Don't use default TLD of vagrant.test
  # This seems to cause issues with packets intermittently dropping for some reason?
  config.landrush.tld = 'vm'

  # VM for testing
  config.vm.define "test" do |n|
    n.vm.provider provider do |v|
      v.memory = 1024
      v.cpus = 1
    end

    n.vm.hostname = "test"
    n.vm.network "private_network", ip: "192.168.10.60"
  end

  config.vm.define "lb" do |n|
    n.vm.provider provider do |v|
      v.memory = 512
      v.cpus = 1
    end

   n.vm.hostname = "lb"
    n.vm.network "private_network", ip: "192.168.10.30"
    n.vm.network "forwarded_port", guest: 8443, host: 8443, :host_ip => "*", :gateway_ports => true, :adapter => "eth1"
  end

  config.vm.define "nfs" do |n|
    n.vm.provider provider do |v|
      v.memory = 512
      v.cpus = 1
    end

    n.vm.hostname = "nfs"
    n.vm.network "private_network", ip: "192.168.10.31"
  end

  config.vm.define "lbext" do |n|
    n.vm.provider provider do |v|
      v.memory = 512
      v.cpus = 1
    end

    n.vm.hostname = "lbext"
    n.vm.network "private_network", ip: "192.168.10.32"
    n.vm.network :forwarded_port, :guest => 443, :host => 443, :host_ip => "*", :gateway_ports => true, :adapter => "eth1"
    n.vm.network :forwarded_port, :guest => 80, :host => 8080, :host_ip => "*", :gateway_ports => true, :adapter => "eth1"
  end

  config.vm.define "master1" do |n|
    n.vm.provider provider do |v|
      v.memory = 4096
      v.cpus = 2

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "master1"
    n.vm.network "private_network", ip: "192.168.10.10"
  end

  config.vm.define "master2" do |n|
    n.vm.provider provider do |v|
      v.memory = 4096
      v.cpus = 2

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "master2"
    n.vm.network "private_network", ip: "192.168.10.11"
  end

  config.vm.define "master3" do |n|
    n.vm.provider provider do |v|
      v.memory = 4096
      v.cpus = 2

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "master3"
    n.vm.network "private_network", ip: "192.168.10.12"
  end

  config.vm.define "infra1" do |n|
    n.vm.provider provider do |v|
      v.memory = 6144
      v.cpus = 4

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "infra1"
    n.vm.network "private_network", ip: "192.168.10.20"
  end

  config.vm.define "infra2" do |n|
    n.vm.provider provider do |v|
      v.memory = 6144
      v.cpus = 4

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "infra2"
    n.vm.network "private_network", ip: "192.168.10.21"
  end

  config.vm.define "node1" do |n|
    n.vm.provider provider do |v|
      v.memory = 2048
      v.cpus = 2

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "node1"
    n.vm.network "private_network", ip: "192.168.10.22"
  end

  config.vm.define "node2" do |n|
    n.vm.provider provider do |v|
      v.memory = 2048
      v.cpus = 2

      v.storage :file, :size => "10G", :allow_existing => true
    end

    n.vm.hostname = "node2"
    n.vm.network "private_network", ip: "192.168.10.23"
  end
end
